/*
  version.h

  (C) 2018 Henryk Richter <henryk.richter@gmx.net>

  version and date handling
*/
#ifndef _INC_VERSION_H
#define _INC_VERSION_H

#define DEVICEVERSION  1
#define DEVICEREVISION 13
#define DEVICEEXTRA
/* #define DEVICEEXTRA Beta */
#define DEVICEDATE     2024-06-15

#endif
